import faker from 'faker';

export const mount = (element) => {
  element.innerHTML = `<h2>You have ${faker.random.number()} items in your cart</h2>`;
};

if (process.env.NODE_ENV === 'development') {
  const el = document.querySelector('#cart-0xh4lxwi8');
  if (el) {
    mount(el);
  }
}
