import { mount as mountProducts } from 'products/ProductsIndex';
import { mount as mountCart } from 'cart/CartIndex';

console.log('Container');

mountProducts(document.querySelector('#my-products'));
mountCart(document.querySelector('#dev-cart'));
