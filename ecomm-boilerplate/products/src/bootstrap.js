import faker from 'faker';

export const mount = (element) => {
  let products = '';

  for (let i = 0; i < 3; i++) {
    const name = faker.commerce.productName();
    products += `<div>${name}</div>`;
  }

  element.innerHTML = products;
};

// Context 1: Running in development
// Use local index.html which DEFINITELY
// has #dev-products
// IMMEDIATELY render app
if (process.env.NODE_ENV === 'development') {
  const el = document.querySelector('#dev-products');
  // Assuming container does NOT have #dev-products
  if (el) {
    mount(el);
  }
}
