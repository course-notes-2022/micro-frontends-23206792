# Implementing Module Federation

We'll now learn to use our `products` code in our `container`. These are the
steps we'll follow:

![federation steps](./screenshots/federation-steps.png)

1. Designate one app as the Host (`Container`) and one as the Remote
   (`Products`)

2. In the remote, decide which modules (files) you want to make available to
   other projects

3. Set up Module Federation plugin to expose those files:

   - Make the following changes to `products/webpack.config.js`:

   ```javascript
   const HtmlWebpackPlugin = require('html-webpack-plugin');
   const ModuleFederationPlugin = require('webpack/lib/container/  ModuleFederationPlugin');

   module.exports = {
     mode: 'development',
     devServer: {
       port: 8081,
     },
     plugins: [
       new HtmlWebpackPlugin({
         template: './public/index.html',
       }),
       new ModuleFederationPlugin({
         name: 'products',
         filename: 'remoteEntry.js',
         exposes: {
           './ProductsIndex': './src/index',
         },
       }),
     ],
   };
   ```

4. In the host, decide which files you want to get from the remote

5. Set up Module Federation plugin to fetch those files

   - In `container/webpack.config.js`:

   ```javascript
   const HtmlWebpackPlugin = require('html-webpack-plugin');
   const ModuleFederationPlugin = require('webpack/lib/container/  ModuleFederationPlugin');

   module.exports = {
     mode: 'development',
     devServer: {
       port: 8080,
     },
     plugins: [
       new HtmlWebpackPlugin({
         template: './public/index.html',
       }),
       new ModuleFederationPlugin({
         name: 'container',
         remotes: {
           products: 'products@http:// localhost:8081/remoteEntry.js',
         },
       }),
     ],
   };
   ```

6. In the host refactor the entry point to load asynchronously:

   - Rename your **current** `index.js` file in `container` to
     `container/src/bootstrap.js`.

   - Create a **new** `index.js` file with the following content:

   ```javascript
   import('./bootstrap');
   ```

7. In the host, import whatever files you need from the remote:

   - In `bootstrap.js`:

   ```javascript
   import 'products/ProductsIndex';

   console.log('Container');
   ```

In `container/public/index.html`, add a `<div>` with the `id` attribute =
"dev-products".

In the terminal, **restart your Webpack processes**. Navigate to
`localhost:/8080`. Note that we're running the `container` application, **but we
can see our list of products created in the `products` application!**
