# Understanding Configuration Options

`container/webpack.config.js`:

```javascript
module.exports = {
  // ...
  plugins: [
    new ModuleFederationPlugin({
      name: 'container', // Not used, added for clarity and convention. Only needed for remotes
      remotes: {
        // Lists projects container can search to get additional code
        products: 'products@http://localhost:8081/remoteEntry.js', // load file at the listed URL if anything in Container has an import like: `import abc from 'products'
      },
    }),
  ],
};
```

Note that the `products` attribute name on the `remotes` object in
`container/webpack.config`:

`products: "products@http://localhost:8081/remoteEntry.js";`

Matches 'name' property of the `ModuleFederationPlugin` config object in
`products/webpack.config.js`:

`products/webpack.config.js`

```javascript
module.exports = {
  // ...
  plugins: [
    new ModuleFederationPlugin({
      name: 'products',
      filename: 'remoteEntry.js', // leave as 'remoteEntry.js' unless you have a reason not to
      exposes: {
        // Controls which files in products we expose to the outside world
        './ProductsIndex': './src/index', // Aliasing; Returns the ''./src/index' file anytime container requests ''./ProductsIndex'
      },
    }),
  ],
};
```

And that **both attributes** match the `import` statement in `bootstrap.js`:

```js
import 'products/ProductsIndex';
```
