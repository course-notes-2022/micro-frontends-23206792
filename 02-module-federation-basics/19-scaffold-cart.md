# Scaffolding the Cart

Create a new `cart` app along the same lines of `products`. Use `products` as a
guide:

`webpack.config.js`:

```js
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');

module.exports = {
  mode: 'development',
  devServer: {
    port: 8082,
  },
  plugins: [
    new ModuleFederationPlugin({
      name: 'cart',
      filename: 'remoteEntry.js',
      exposes: {
        './CartShow': './src/index',
      },
    }),
    new HtmlWebpackPlugin({
      template: './public/index.html',
    }),
  ],
};
```

`src/index.js`:

```js
import faker from 'faker';

const cartText = `<div>You have ${faker.random.number()} items in your cart</div>`;

document.querySelector('#cart').innerHTML = cartText;
```

`public/index.html`:

```html
<!DOCTYPE html>
<html>
  <head></head>
  <body>
    <div id="cart"></div>
  </body>
</html>
```
