# More on Module Federation

Now let's take a look at what the `ModuleFederationPlugin` does in the
`container`:

![more module federation](./screenshots/more-module-federation.png)

Note that we created the `bootstrap.js` file that contains our application code,
and that `index.js` now imports `bootstrap.js` via this strange syntax:

```javascript
import('./bootstrap');
```

`index.js` allows Webpack to **fetch code from the `products` project** before
executing the code in `bootstrap.js`.

## Putting Federation Together

![putting federation together](./screenshots/putting-federation-together.png)
