# Understanding Module Federation

Let's understand what's happening in the **`products`** module federation only
for now:

![products module federation](./screenshots/products-module-federation.png)

Webpack is outputting **two sets** of bundles: `main.js` allows us to run
`products` as a standalone app.

The `ModuleFederationPlugin` outputs 3 files:

- `remoteEntry.js`: Contains a list of files that are available from this
  project, plus directions for `container` on how to load them

- `src_index.js`: Version of src/index.js that has been processed by Webpack and
  can be safely loaded in the browser

- `faker.js`: version of `faker` dependency that has been processed by Webpack
  that can be safely loaded in the browser
