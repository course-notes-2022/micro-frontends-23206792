# Cart Integration

- Update the `container/webpack.config.js` with a new `ModuleFederationPlugin`:

```javascript
plugins: [
    new HtmlWebpackPlugin({
      template: './public/index.html',
    }),
    new ModuleFederationPlugin({
      name: 'container',
      remotes: {
        products: 'products@http://localhost:8081/remoteEntry.js',
        cart: 'cart@http://localhost:8082/remoteEntry.js',
      },
    }),
  ],

```

- Add a `div` with the id attributed equal to `dev-cart`:

```html
<!DOCTYPE html>
<html>
  <head></head>
  <body>
    <div id="dev-products"></div>
    <div id="dev-cart"></div>
  </body>
</html>
```

**Note:** The `id` attribute _cannot be equal to the name of the container_.

Restart the applications and visit `localhost:8080`. You should be able to see
your products _and_ cart!
