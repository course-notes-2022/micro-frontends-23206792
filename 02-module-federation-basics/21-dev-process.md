# The Development Process

Let's take a look at how we'd develop a project like this in the real world:

![development process](./screenshots/dev-process.png)

Note that teams 1 & 2 can develop their apps **in isolation**, as long as they
make use of the `ModuleFederationPlugin`. Team 3 needs to make sure that all 3
projects are running (i.e. running `npm run start`). We'll look at the
container's role in more detail later.

Also note that the `products` and `cart` HTML files are used **only during
development** of those apps, i.e. to load them in a browser **in isolation**.
Only the HTML file in `container` will be used **in production**.
