# Some Background on Webpack

- Create a file in the root of the `products` directory called
  `webpack.config.js`. Add the following content:

```javascript
module.exports = {
  mode: 'development',
};
```

- Add the following script to `package.json`:

```json
{
  "scripts": {
    "start": "webpack"
  }
}
```

- Run `npm run start` from a terminal window. Note that webpack creates a `dist`
  folder with a `main.js` file in your project root.

## What Just Happened?

**Webpack** is a **module bundler**. It takes our code **dependencies**, along
with our **application source code**, and **bundles them up** into one (or a
few) Javascript files. This allows us to load as **few files as possible** in
the browser.

![webpack](./screenshots/webpack.png)
