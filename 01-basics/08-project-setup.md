# Project Setup

For this first iteration each of our 3 apps will be built with plain JS (no
framework). Each will be able to run in isolation, and together through the
"container" app. We'll focus on `ProductsList` first.

In a terminal window, create a new directory to hold all the subprojects,
`ecomm`. Inside `ecomm`, create another folder `products`.

`cd` into `products` and run `npm init`. Install the following npm packages:

```
npm install webpack@5.88.0 webpack-cli@4.10.0 webpack-dev-server@4.7.4 faker@5.1.0 html-webpack-plugin@5.5.0
```
