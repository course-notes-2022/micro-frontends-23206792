# Understanding Build-Time Integration

**Integration** refers to how and when the Container gets access to the source
code in MFE app #1 and #2.

Note that there are many different ways to integrate. Generally, there are 3
major categories:

1. Build Time (Compile Time) Integration: **Before** the container gets loaded
   in the browser, it gets access to the ProductsList and Cart source code:

![build time integration](./screenshots/build-time.png)

2. Runtime (Client Side) Integration: **After** the container gets loaded in the
   browser, it gets access to the ProductsList and Cart source code

![runtime integration](./screenshots/runtime-integration.png)

3. Server Integration: While sending down JS to load the container, a server
   decides whether or not to include ProductsList source
