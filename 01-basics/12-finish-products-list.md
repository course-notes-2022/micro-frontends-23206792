# Finishing the Product List

Let's now display our list of products to the user in our HTML template.

```html
<!DOCTYPE html>
<html>
  <head></head>
  <body>
    <div id="dev-products"></div>
  </body>
</html>
```

```javascript
import faker from 'faker';

let products = '';

for (let i = 0; i < 3; i++) {
  const name = faker.commerce.productName();
  products += `<div>${name}</div>`;
}

document.querySelector('#dev-products').innerHTML = products;
```

We can now see our products output in the template. This is progress, but
currently `products` only works as a standalone app. Next, we'll learn to plan
to integrate `products` with the `container` app, once we build `container`.
