# More on Webpack

We now want to load and execute `main.js` in the browser. To do so we'll setup
`webpack-dev-server`. `webpack-dev-server` takes the output from the bundling
process and serves it up to the browser:

`webpack.config.js`:

```javascript
module.exports = {
  mode: 'development',
  devServer: {
    port: 8081,
  },
};
```

`package.json`:

```json
{
  "scripts": {
    "start": "webpack serve"
  }
}
```

`/public/index.html`:

```html
<!DOCTYPE html>
<html>
  <head></head>
  <body></body>
</html>
```

We also need to configure `WebpackHTMLPlugin`. This is necessary to
**automatically insert** the generated javascript bundle files into our HTML
document, i.e. create a `<script>` tag in our HTML document with the `src`
attribute equal to the (generated) name(s) of our bundled application:

`webpack.config.js`:

```javascript
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
  devServer: {
    port: 8081,
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './public/index.html',
    }),
  ],
};
```
