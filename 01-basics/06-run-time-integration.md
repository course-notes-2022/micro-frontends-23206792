# Runtime Integration

In this course, we will focus on implementing **runtime integration** using
Webpack and **module federation**:

![runtime integration](./screenshots/runtime-integration.png)

It's the most flexible and performant solution right now, and it makes sense to
cover it in great detail.

## Upsides and Downsides of Runtime Integration

**Upsides**:

- `ProductsList` and `Cart` can be deployed independently at any time
- Different versions of `ProductsList` and `Cart` can be deployed and
  `Container` can decide which one to use

**Downsides**:

- Tooling and setup is (relatively) more complicated
- We have to spend a lot of time on Webpack and how it works
