# App Overview

For our first app, we'll build a simple e-commerce store:

- Products for Sale:

  - Chair
  - Table
  - Lamp

- Cart
  - "You have one item in your cart"

![sample app](./screenshots/sample-app.png)

Note that:

- The data is 100% fake
- No interaction (no adding items to cart)

We'll create two microfronted apps: one for the product list, and one for the
cart. We'll create a third "container" app to decide when to show which of the
other two apps:

![sample app microfrontend](./screenshots/sample-app-micro2.png)
