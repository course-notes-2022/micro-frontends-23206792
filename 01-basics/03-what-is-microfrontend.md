# What is a Microfrontend?

Microfrontends split functionality that is commonly included in a monolithic SPA
into multiple, smaller apps:

![app separation](./screenshots/app-separation.png)

We usually want to limit direct communication between apps as much as possible.
Instead, we make use of APIs.

![limit direct communication](./screenshots/limiting-direct-communication.png)

# Summary

Microfrontends divid a monolithic app into multiple, smaller apps. Each smaller
app is responsible for a distinct feature of the product. This allows **multiple
engineering teams** to work in isolation, and each smaller app is easier to
understand/change.
