# Generating Products

Let's create a `src/index.js` file in our products directory and use `faker` to
generate some fake products:

`index.js`:

```javascript
import faker from 'faker';

let products = '';

for (let i = 0; i < 3; i++) {
  const name = faker.commerce.productName();
  products += `<div>${name}</div>`;
}

console.log(products);
```
