# Scaffolding the Container

- Install the package dependencies in the `container` directory and add the
  `start` script:

```json
{
  "name": "container",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "start": "webpack serve"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "html-webpack-plugin": "^5.5.0",
    "nodemon": "^3.0.1",
    "webpack": "^5.88.0",
    "webpack-cli": "^4.10.0",
    "webpack-dev-server": "^4.7.4"
  }
}
```

- Create `public/index.html`:

```html
<!DOCTYPE html>
<html>
  <head></head>
  <body></body>
</html>
```

- Create `src/index.js`.

```js
console.log('container');
```

- Create `webpack.config.js:

```js
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
  devServer: {
    port: 8080,
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './public/index.html',
    }),
  ],
};
```

Run the `npm start` script to start the dev server, and verify that you can see
"container" logged to the console.
