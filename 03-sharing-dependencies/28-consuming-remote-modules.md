# Consuming Remote Modules

`container/src/index.js`:

```javascript
import mount from 'products/ProductIndex';
import 'cart/CartIndex';

console.log('Container');

// Context 2: Running in dev or prod
// through the CONTAINER app
// No guarantee that #dev-products exists
// DO NOT immediately render app
mount(document.querySelector('#my-products'));
```

`container/public/index.html`:

```html
<!DOCTYPE html>
<html>
  <head></head>
  <body>
    <div id="my-products"></div>
    <div id="dev-cart"></div>
  </body>
</html>
```

`products/webpack.config.js`:

```javascript
ts = {
  plugins: [
    new ModuleFederationPlugin({
      name: 'products',
      filename: 'remoteEntry.js',
      exposes: {
        './ProductsIndex': './src/bootstrap',
      },
      shared: ['faker'],
    }),
  ],
};
```
