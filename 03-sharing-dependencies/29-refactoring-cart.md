# Refactoring `cart`

Let's refactor `cart` along the same lines as `product` in order to ensure it
runs in isolation and in `container`, where it may not find the expected HTML
element target. Add the following to `cart/src/bootstrap.js`:

```js
import faker from 'faker';

const mount = (el) => {
  const cartText = `<div>You have ${faker.random.number()} items in your cart</div>`;
  el.innerHTML = cartText;
};

if (process.env.NODE_ENV === 'development') {
  const el = document.querySelector('dev-cart');
  if (el) {
    mount(el);
  }
}

export { mount };
```

Ensure that `cart/webpack.config.js` exposes `bootstrap.js`:

```js
module.exports = {
  mode: 'development',
  devServer: {
    port: 8082,
  },
  plugins: [
    new ModuleFederationPlugin({
      name: 'cart',
      filename: 'remoteEntry.js',
      exposes: {
        './CartShow': './src/bootstrap',
      },
      // ...
  ],
};
```

In `container`, import the `mount` function exported by `products` _and_ `cart`,
and call them with the appropriate element references:

```js
import { mount as productMount } from 'products/ProductsIndex';
import { mount as cartMount } from 'cart/CartShow';

productMount(document.querySelector('#my-products'));
cartMount(document.querySelector('#my-cart'));
```

Add a `<div id="my-cart">` element to the `container` template:

```html
<!DOCTYPE html>
<html>
  <head></head>
  <body>
    <div id="my-products"></div>
    <div id="my-cart"></div>
  </body>
</html>
```
