# Singleton Loading

Some modules, such as React, **cannot be loaded multiple times in the browser**.
There might be scenarios where we might have different versions of a package,
but want to **ensure we load only a single copy no matter what**:

`cart/webpack.config.js`

```javascript
module.exports = {
  plugins: [
    new ModuleFederationPlugin({
      name: 'cart',
      filename: 'remoteEntry.js',
      exposes: {
        './CartIndex': './src/index.js',
      },
      shared: {
        faker: {
          singleton: true,
        },
      },
    }),
  ],
};
```

`products/webpack.config.js`:

```javascript
module.exports = {
  plugins: [
    new ModuleFederationPlugin({
      name: 'products',
      filename: 'remoteEntry.js',
      exposes: {
        './ProductsIndex': './src/index.js',
      },
      shared: {
        faker: {
          singleton: true,
        },
      },
    }),
  ],
};
```

Webpack will put a **warning** in the console if conflicting versions exist.
Only one copy will be loaded in the browser. You will need to debug and decide
if you **really need** multiple versions of the same package.
