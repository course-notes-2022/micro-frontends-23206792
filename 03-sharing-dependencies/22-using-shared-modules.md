# Using Shared Modules

Let's look at how to share dependencies **between** projects. Note that **both**
cart and `products` are loading the `faker` module. This is redundant and
consumes extra bandwidth.

## A Better Approach

![sharing modules](./screenshots/sharing-modules-steps.png)

We want to notify `container` that both sub-apps need `faker`. The container can
then load only a **single** copy of `faker` and make it available to **both
modules**. We just need to change a couple of config options:

`products/webpack.config.js`:

```javascript
module.exports = {
  // ...
  plugins: [
    new ModuleFederationPlugin({
      name: 'products',
      filename: 'remoteEntry.js',
      exposes: {
        './ProductsIndex': './src/index',
      },
      shared: ['faker'], // 'shared' indicates shared modules
    }),
  ],
};
```

`cart/webpack.config.js`:

```javascript
module.exports = {
  // ...
  plugins: [
    new ModuleFederationPlugin({
      name: 'cart',
      filename: 'remoteEntry.js',
      exposes: {
        './CartIndex': './src/index',
      },
      shared: ['faker'], // 'shared' indicates shared modules
    }),
  ],
};
```

Note that this **works** for the entire app in the container, but **breaks the
individual apps**. We need to load the `faker` module **asynchronously** in the
sub-apps.
