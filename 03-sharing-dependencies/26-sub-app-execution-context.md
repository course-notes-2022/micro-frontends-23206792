# Sub-App Execution Context

We should be able to develop each project in isolation, and run each project in
isolation _as well as in `container`_.

Right now, we are **assuming** that the container `div` elements we've
identified in our HTML files are **actually there**. That's a safe assumption
for the **sub-app** files, but the **container** HTML file may **not** have that
`div` (especially if the development is being done by separate teams)!

`products/src/bootstrap.js`:

```javascript
import faker from 'faker';

// ...

document.querySelector('#dev-products').innerHTML = products;

// Context 1: Running in development
// Use local index.html which DEFINITELY
// has #dev-products
// IMMEDIATELY render app

// Context 2: Running in dev or prod
// through the CONTAINER app
// No guarantee that #dev-products exists
// DO NOT immediately render app
```
