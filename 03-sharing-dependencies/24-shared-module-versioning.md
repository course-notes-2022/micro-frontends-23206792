# Shared Module Versioning

Now, `cart` and `products` are using the same `faker` module. But recall that
these apps could be developed by **different teams**. What if each team wanted
to used a **different version** of `faker`?

`ModuleFederationPlugin` looks at the versions inside `package.json`, and is
smart enough to **load different versions if package versions in `package.json`
don't match** (according to the `semver` matching rules).
