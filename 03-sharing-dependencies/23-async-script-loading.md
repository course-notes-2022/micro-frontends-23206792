# Async Script Loading

Create a new file in the `products/src` directory, `bootstrap.js`. \*\*Cut the
_current_ contents from `index.js` and paste into `bootstrap`:

`products/src/bootstrap.js`

```javascript
import faker from 'faker';

let products = '';

for (let i = 0; i < 3; i++) {
  const name = faker.commerce.productName();
  products += `<div>${name}</div>`;
}

document.querySelector('#dev-products').innerHTML = products;
```

Add the following to `index.js`:

`products/src/index.js`:

```javascript
import('./bootstrap');
```

This allows Webpack to load the `bootstrap.js` file **asynchronously**. This
gives Webpack the opportunity to realize that before we run this code in the
browser, we need to load `faker`.

Repeat the above steps in `cart`, and restart the applications.
