# Requirements That Drive Architecture Choices

**_Disclaimer_**: There are **multiple micro-frontend architectures** out there;
the way we accomplish our goal **here** may be **different** from what you see
elsewhere (though it still conforms to accepted best practices). The
architecture for **this project** is determined by its _requirements_. You need
to think about the requirements of your app and decide the **appropriate
architecture**.

## Some Techniques You May See Others Advocate

- "Share state between apps with Redux"
- "The container must be written with web components"
- "Each microfrontend can be a React component that is directly used by another
  app"
- "Communicate betwen apps using XYZ system only"

These aren't _wrong_, they just don't correspond to the requirements of _our
project_.

## Our Requirements

1. **Zero coupling between child projects**
   - No importing of functions/objects/classes/etc
   - No shared state
   - Shared libraries through module federation is OK

Imagine that the marketing and authentication apps are **tightly coupled**. What
if later on we decide to replace marketing with an app built in a **new
framework**? We could easily **break** authentication!

2. **Near-zero coupling between container and child apps**
   - Container shouldn't assume that a child is using a particular framework
   - Any necessary communication done with callbacks or simple events

We need to have _some_ communication between the child and the parent.

3. **CSS from one project shouldn't affect another project**.

4. **Version control (monrepo vs separate) shouldn't have any impact on the
   overall project**.

   - Some people want to use monorepos
   - Some want to keep everything in a separate repo
   - Everything should work either way

5. **Container should be able to decide to always use the latest version of a
   microfrontend _or_ a _specific version_**.
   - Container will always use the latest version of a child app(doesn't require
     a redeploy of container), _OR_:
   - Container can specify exactly what version of a child it wants to use
     (requies a redeploy to change)
