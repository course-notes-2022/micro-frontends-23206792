# Running `marketing` in Isolation

Create a `src` and `public` directory inside `marketing`. Create the following
files:

`public/index.html`:

```html
<!DOCTYPE html>
<html>
  <head></head>
  <body></body>
</html>
```

`src/index.js`:

```js
console.log('hello from marketing');
```

Add the `start` script to your `package.json` file:

```json
{
  "name": "marketing",
  "version": "1.0.0",
  "scripts": {
    "start": "webpack serve--config config/webpack.dev.js"
  }
}
```

Note that we are setting the location of the `config` file we want `marketing`
to use with the `--config config/webpack.dev.js` option.

Start the project in a terminal window, and visit `localhost:8081`. Verify that
the application starts.
