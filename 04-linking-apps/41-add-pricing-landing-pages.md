# Add `pricing` and `landing-page` Components

Unzip the `.zip` file, and move them to the `src` file inside `marketing`. The
contents contain boilerplate for the `Pricing` and `LandingPage` components. We
want to focus on the act of building microfrontends, and not the ins and outs of
any particular framework.
