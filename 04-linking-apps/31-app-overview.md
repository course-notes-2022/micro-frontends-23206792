# Application Overview

Our first app turned out well, but we need to go further. In this chapter we'll
begin our first "real" application. This is a **strictly front-end**
application; the components will not "do" anything or work with any actual data.

Our app will consist of:

1. Home/Landing Page (`app.com/`)
2. Pricing Page (`app.com/pricing`)
3. Sign In Page (`app.com/signin`)
4. Sign Up Page (`app.com/signup`)
5. Dashboard Page (`app.com/dashboard`)

Think about how you would group these pages together in terms of functionality?

![grouping app pages](./screenshots/grouping-app-pages.png)

We might decide to create 3 different engineering teams to take charge of the
Marketing, Authentication, and Dashboard apps. Let's continue to plan in the
next section.
