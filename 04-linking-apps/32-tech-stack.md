# Tech Stack

Let's create 3 different microfrontends, and a container application along the
lines we discussed in the previous chapter:

![app plan](./screenshots/app-plan.png)

We'll use React for the container, marketing, and auth apps, and Vue for the
dashboard. Our goal is to learn a **generic coupling method** that will easily
allow us to **swap frameworks** in the future.
