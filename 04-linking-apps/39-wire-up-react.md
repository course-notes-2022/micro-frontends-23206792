# Wiring Up React

We're now ready to do some development using React in our `marketing` app.

Recall from our previous app, we tried to share a dependency on `faker`. This
caused an error whenever we tried to load the `cart` or `products` apps _in
isolation_. This error was related to the fact that we were trying to use
`faker` **before** it was loaded in the browser.

To get around this, we created `index.js` that imported `bootstrap.js`:

`bootstrap.js`:

```js
import faker from 'faker';

const mount = (el) => {
  const cartText = `<div>You have ${faker.random.number()} items in your cart</div>`;
  el.innerHTML = cartText;
};

if (process.env.NODE_ENV === 'development') {
  const el = document.querySelector('dev-cart');
  if (el) {
    mount(el);
  }
}

export { mount };
```

`index.js`:

```js
import('./bootstrap');
```

**The only goal of `index` was to add in the `import` function, giving webpack
in the browser a chance to load `faker` _before_ `index.js` was loaded in the
browser.**

We'll do the same in our current projects. In `marketing`, create a
`src/bootstrap.js` file alongside `index`:

`bootstrap.js`

```js
import React from 'react';
import ReactDOM from 'react-dom';

// Mount function to start up the app
const mount = (el) => {
  ReactDOM.render(<h1>Hello From Marketing</h1>, el);
};

// If we are in development and in isolation
// call mount immediately
if (process.env.NODE_ENV === 'development') {
  const devRoot = document.querySelector('#_marketing-dev-root');

  if (devRoot) {
    mount(devRoot);
  }
}

// We are running through container
// and we should export the mount function
export { mount };
```

Modify `index` to import `bootstrap` using the `import` **function**:

`index.js`:

```js
import('./bootstrap');
```

Modify `index.html` to add an `id` attribute to the root element:

```html
<!DOCTYPE html>
<html>
  <head></head>
  <body>
    <!--choose an 'id' attribute that is UNLIKELY to appear in the container template-->
    <div id="_marketing-dev-root"></div>
  </body>
</html>
```

Save and restart the application. Note that you should see the "Hello from
Marketing" output in the browser.
