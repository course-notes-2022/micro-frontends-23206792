# Initial Webpack Config

After installing all dependencies in each of the four subprojects, let's now
begin to connect each project from scratch. Why no `create-react-app` or
`vue cli`? Currently, neither support a version of Webpack that support the
module federation plugin (though that may change in the future). Also, we have a
lot of dependencies that we want to ensure have **not changed** by the time you
begin this lesson.

## What We'll Do

We'll attempt to get our **marketing** project and webpack config working first,
both for **development** and **production**. This will require two separate
webpack config files. We'll create a 3rd config file that is **common** for both
dev and prod environments.

Create a new folder `config` inside of `marketing`. Create 3 new files,
`webpack.common.js`, `webpack.dev.js`, and `webpack.prod.js` inside. Add the
following to `webpack.common.js`:

```js
module.exports = {
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-react', '@babel/preset-env'],
            plugins: ['@babel/plugin-transform-runtime'],
          },
        },
      },
    ],
  },
};
```

- A **loader** instructs webpack how to process a file in our application.
- `babel-loader` processes our ES2016, 2017, 2018... code into ES5 code that can
  be executed in any modern browser.
- `test: /\.m?js$/`: Instructs babel to process any file that ends with a `.js`
  or `.mjs` extension
