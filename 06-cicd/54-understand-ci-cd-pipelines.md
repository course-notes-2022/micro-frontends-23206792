# Understanding CI/CD Pipelines

Our CI/CD pipeline will automatically build our subproject whenever we push
changes to our Github repository, and then attempt to deploy that change to our
AWS infrastructure. We'll use **Github Actions** to implement our CI/CD
pipeline.

![github actions](./screenshots/github-actions.png)

Our workflow will look like the following:

1. Whenever code is pushed to the `main` branch _and_ this commit contains a
   change to `subprojectA`:
2. Change into the folder for `subprojectA`
3. Install dependencies
4. Create a `production` build with webpack
5. Upload the result to AWS S3

These commands are executed in a **virtual machine** that is created, hosted,
and destroyed by Github for us.
