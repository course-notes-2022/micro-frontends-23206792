# Initial Git Setup

1. Create a repo on Github
2. Create a local git repo, set up `.gitignore` and remotes
3. Push code (quick test)
4. Set up the webpack production config
5. Set up the CI/CD pipeline (executed through github)
