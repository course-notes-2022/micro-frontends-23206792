# Requirements for Deployment

Let's begin thinking about **deploying** our application. We've only gotten a
little bit of the application done, but certain challenges will arise with
deployment that will affect how we'll build our application(s). Let's deal with
them now.

## What Are Our Requirements?

1. Must be able to deploy each microfrontend independently (including the
   container)
2. Location of child app `remoteEntry.js` files must be known at **build time**
3. Many front-end deployment solutions assume you're deploying a single project;
   we need something that can handle multiple different ones
4. Probably need a CI/CD pipeline of some sort
5. At present, the `remoteEntry.js` file name is fixed! Need to think about
   caching issues
