# Testing the Pipeline

Let's test our action out. Note that the action will _fail_, as we have not yet
set up our AWS infrastructure, but let's ensure that the action _runs_.

Commit your changes and push them up to the `main` branch. In the browser, find
the `Actions` tab and click it. Verify that your workflow is running/has run.
