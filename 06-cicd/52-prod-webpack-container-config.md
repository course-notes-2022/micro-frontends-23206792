# Production Configuration for `container`

Let's begin to add our `prod` config for the `container` application.

Add the following to `webpack.prod.js`:

```js
const { merge } = require('webpack-merge');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const commonConfig = require('./webpack.common');
const packageJson = require('../package.json');

const domain = process.env.PRODUCTION_DOMAIN;

const prodConfig = {
  mode: 'production',
  output: {
    filename: '[name].[contenthash].js',
  },
  plugins: [
    new ModuleFederationPlugin({
      name: 'container',
      remotes: {
        marketing: `marketing@${domain}/marketing/remoteEntry.js`,
      },
      shared: packageJson.dependencies,
    }),
  ],
};

module.exports = merge(commonConfig, prodConfig);
```

**Note the following**:

1. The `filename` property is now a `dynamic` value combining the `name` of the
   **original** JS file, and a **hash** of the file's contents. This ensures
   that all the built production files have a unique name to avoid **caching
   issues** (more on them later).
2. The `remotes` property points `marketing` to a **dynamic** value representing
   the location of the `remoteEntry` file in its **deployed, production
   domain**. We _don't yet know what this domain is yet_ as we haven't yet
   created it. Therefore, we are using the value of
   `process.env.PRODUCTION_DOMAIN`, which is a variable _that we will set as an
   environment variable_ in our CI/CD pipeline, once we've set up our deployment
   infrastructure.

Add the following to `webpack.common.js`:

```js
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-react', '@babel/preset-env'],
            plugins: ['@babel/plugin-transform-runtime'],
          },
        },
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './public/index.html',
    }),
  ],
};
```

**Note that we have _removed_ `HtmlWebpackPlugin`** from the `dev` config file
and refactored it to `common`. Since both the `dev` _and_ `prod` configurations
will need to generate `index.html`, it makes more sense to refactor it to
`common`.

Add the `"build"` script to `package.json`:

```json
{
  "name": "container",
  "version": "1.0.0",
  "scripts": {
    "start": "webpack serve --config config/webpack.dev.js",
    "build": "webpack --config config/webpack.prod.js"
  }
}
```

Run `npm run build` and ensure the `production` build runs without errors.
