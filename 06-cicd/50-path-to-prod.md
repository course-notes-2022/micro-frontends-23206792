# Path to Production

Let's look at the overall flow we'll use to deploy our app.

We'll set up a Git repository as a **monorepo** (many sub-projects in a single
repository). This isn't _mandatory_; you _could_ create multiple repos for each
project. In this course we're using a monorepo for simplicity.

We'll push all our code to Github. We'll have a **script** that looks for any
**changes** in any of our **subprojects**. If _yes_ we'll kick off a build that
builds **prod** version of the _changed_ app. We'll then upload the built
version to S3. Notet that each process is **independent** of the others; a
change in project `A` will kick off the build process for `A` **only**.

Our application(s) will be served by **Amazon CloudFront**. Note that you _will_
need an AWS account to complete this deployment exercise.

![serve from cloudfront](./screenshots/serve-from-cloudfront.png)
