# Creating the `container` Action

Let's create our first workflow to deploy our `container`. We'll write our
workflow file inside our project.

Create a folder in the project root called `.github/workflows`. Create a
`container.yml` file inside `workflows`. Github will automatically look for a
`yaml` file in this location. Add the following content:

```yaml
name: deploy-container

on:
  push:
    branches:
      - main
    paths:
      - 'packages/container/**' # run workflow only when something in 'container' changes

defaults:
  run:
    working-directory: packages/container

jobs:
  build:
    runs-on: ubuntu-latest # choose the type of VM

    steps:
      - uses: actions/checkout@v2
      - run: npm install
      - run: npm run build

      - uses: shinyinc/action-aws-cli@v1.2
      - run:
          aws s3 sync dist s3://${{ secrets.AWS_S3_BUCKET_NAME
          }}/container/latest
        env:
          AWS_ACCESS_KEY_ID: ${{ secrets.AWS_ACCESS_KEY_ID }}
          AWS_SECRET_ACCESS_KEY: ${{ secrets.AWS_SECRET_ACCESS_KEY}}
```

Save the `container.yaml` file.
