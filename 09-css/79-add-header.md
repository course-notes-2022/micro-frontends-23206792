# Adding a Header

Extract the `header.zip` file. Place the `Header` component inside
`container/src`.

Make the following changes in `App.js`:

```js
import React from 'react';
import { BrowserRouter } from 'react-router-dom/cjs/react-router-dom.min';
import MarketingApp from './components/MarketingApp';
import Header from './Header';

export default () => {
  return (
    <BrowserRouter>
      <div>
        <Header />
        <MarketingApp />
      </div>
    </BrowserRouter>
  );
};
```

Save the changes and restart the applications. You should see the header appear
at the top of the screen.

There are some distinct problems that will arise is **production** that do not
arise in **dev**, many of which are a result of problems related to
microfrontend apps. We'll look at some of those problems with CSS in this
lesson.

Commit the changes and push to the github repo. Once the deployment for
`container` completes (since that is the only version that changed), view the
**deployed** version of the app in a browser.
