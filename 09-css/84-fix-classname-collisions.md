# Fixing Class Name Collision

Fortunately, the fix is pretty simple. We can make sure that the generated class
name is sufficiently random such that we can avoid collision.

In `packages/marketing/src/App.js`, note that we have the `<StylesProvider>`,
which is a React component that we can use to customize the CSS in JS
generation. Make the following changes:

```js
import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import { StylesProvider, createGenerateClassName } from '@material-ui/core';
import Landing from './components/Landing';
import Pricing from './components/Pricing';

const generateClassName = createGenerateClassName({
  productionPrefix: 'ma', // generates all class names with a prefix of `ma` for PRODUCTION, in our `marketing` app ONLY
});

export default () => {
  return (
    <div>
      <StylesProvider generateClassName={generateClassName}>
        <BrowserRouter>
          <Switch>
            <Route exact path="/pricing" component={Pricing} />
            <Route path="/" component={Landing} />
          </Switch>
        </BrowserRouter>
      </StylesProvider>
    </div>
  );
};
```

Let's do the same thing in `container/src/App.js` as well:

```js
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import {
  StylesProvider,
  createGenerateClassName,
} from '@material-ui/core/styles';
import MarketingApp from './components/MarketingApp';
import Header from './Header';

const generateClassName = createGenerateClassName({
  productionPrefix: 'co',
});
export default () => {
  return (
    <BrowserRouter>
      <StylesProvider generateClassName={generateClassName}>
        <div>
          <Header />
          <MarketingApp />
        </div>
      </StylesProvider>
    </BrowserRouter>
  );
};
```

Now **each project is sufficiently namespaced** to avoid collision!
