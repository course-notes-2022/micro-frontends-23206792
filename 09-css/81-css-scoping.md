# CSS Scoping Techniques

Let's look at some different ways of solving this problem. All solutions will
involve **scoping** our CSS:

| Custom CSS You Write for Your Project          | CSS From a Component or CSS Library                                    |
| ---------------------------------------------- | ---------------------------------------------------------------------- |
| Use a CSS-in-JS library                        | Use a component library that makes use of CSS-in-JS                    |
| Use Vue's built-in component style scoping     | Manually build th e CSS library and apply namespacing techniques to it |
| Use Angular's built-in component style scoping |                                                                        |
| "Namespace" all your CSS                       |                                                                        |

## CSS-in-JS

You write some JS code that **generates** CSS with randomly-generated class
names, that get applied to **only the particular elements** where it's needed:

![css in js](./screenshots/css-in-js.png)

## Frameworks

Many frameworks already provide scoping for CSS. You can take advantage of these
libraries for a quick solution to CSS scoping issues in MFEs.

## Namespacing

You can also accomplish scoping **manually** by simply adding a class name to
your elements:

![namespacing](./screenshots/namespacing.png)

## Using a Component Library

You can use component libraries such as Bootstrap or Material UI that make use
of CSS in JS. _In theory_, these should solve the issues for us.

## Remember the Point of MFEs

We always want to ensure that **each subproject is a standalone project** that
can be developed and deployed in isolation.
