# Verifying the Fix

Save the changes and push to the Github repo. Verify that the styling issues are
fixed by inspecting the deployed app in production.

Keep in mind that **other CSS in JS libraries might have better class name
generation**, or **a different way of customizing the class name**. Make sure
you _**check the documentation for your library**_ for the appropriate solution.
