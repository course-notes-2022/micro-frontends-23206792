# So, What's the Bug?

Recall that `makeStyles` generates a **pseudo-random** class name for each
property in the object it returns. Material UI (and many other CSS in JS
libraries you may use) make a **_big assumption_**:

> In _production only_, we might want to extract this generated CSS selector and
> rule to a separate stylesheet.

**To make the generated CSS as small as possible for production**, the CSS in JS
library turns the selector into something like `jss1`, `jss2`, `jss3` etc.

## The Issue

The issue is that **the generated class names are _not sufficiently random_**.
We are building in a MFE architecture where each application is a standalone
app, where multiple submodules make use of the **same CSS in JS library**. If
the library generates **non-unique class names**, and our `container` loads two
projects that define **different rules for the same generated class name**, we
have **collision**:

![css in js classname collision](./screenshots/classnames-collide.png)
