# Understanding CSS in JS Libraries

Let's understand how CSS in JS libraries work.

In `packages/marketing/src/components/Landing.js`, notice that we have a bunch
of imports from MaterialUI, a component library that makes use of Material UI.
Right now, **both** marketing and container are using Material UI:

![material ui in both](./screenshots/material-ui-in-both.png)

Any time you have multiple subprojects using the **same library**, you might run
into **class name collision**.

Inspect the `makeStyles` function in `Landing.js`. Note that the function
returns an object, one property of which is `heroContent`:

```js
const useStyles = makeStyles((theme) => {
    // ...
    heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
})
```

We call `makeStyles`, and 2 things happen:

1. We get an object where `key` is identical to `heroContent`, and `value` is a
   _pseudo-random class name that will be applied to an HTML element_ (for
   example on line 73 `<div className="makeStyles-heroContent-2>`"):

```js
{
  heroContent: 'makeStyles-heroContent-2';
}
```

2. `makeStyles` generates some CSS rules, where the **class name** is equal to
   `value` of the object:

```css
.makeStyles-heroContent-2 {
  padding: 999px;
  //...
}
```
