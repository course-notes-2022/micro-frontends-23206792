# Issues with CSS in Microfrontends

Note that the deployed header looks much too tall. It's clear that there are
some issues between our deployed and production versions, related to CSS. This
is an issue with handling CSS in general in microfrontend apps.

We have a `pricing page` and `signin page` in our app, that are parts of
**separate projects** developed by **separate teams**. We can't guarantee that
we will have strict, agreed-upon requirements between both teams!

Imagine the following scenario: The user starts at the priceing page, and
navigates to the signin page. Upon navigation, authentication loads up some CSS
that contains the following rule:

```css
h1: {
  color: green;
}
```

Our app is a **SPA**, which means we are **not** making new requests for assets
as the user navigates. Signin loads the CSS rule, that will **still be active
once the user navigates _back_ to `pricing`**.

![css issues in mfe apps](./screenshots/css-issues-in-mfe.png)

The issue is that, as the user navigates around our application, if the
sub-modules contain **different** or **conflicting** CSS, _the stylesheets
loaded by submodule B can inadvertently affect elements in submodule A_ even
after the user has navigated _away from B and back to A_.
