# A Few Solutions

Now that we have our requirements, let's look at satisfying them. We'll consider
the first 3 requirements first:

## Requirement 1:

> **Both the `container` and individual sub-apps need routing features.**

We'll use `react-router` as the routing solution for each. We can easily have
two different versions of the library in each application. We could also use
completely different libraries, because communiation **between applications**
will be done as **generically as possible**:

![requirement 1](./screenshots/req-1.png)

## Requirement 2:

> **Sub apps might need to add new pages/routes all the time.**

We'll add some logic in the `container` to decide **which MFE to show**. The
`marketing` router will decide **which page to show**:

![requirement 2](./screenshots/req-2.png)

## Requirement 3:

> **We might need to show two or more microfrontends at the same time.**

Similarly to #2, the `container` routing will decide which MFE to show. It would
be then up to the MFE to decide which page to show:

![requirement 3](./screenshots/req-3.png)
