# Syncing History Objects

In `MarketingApp.js`, let's now figure out where marketing app navigated to, and
**update `container` with the new location**.

As it happens, the `listen` function gives us a `location` object, with
information about where we're about to navigate to. The `location` object looks
like:

```js
{
  pathname: "/pricing",
  search: "",
  hash: "",
  key: "4gp1jg"
}
```

We can use `pathname` to \*\*update the `container` with the path that
`marketing` is attempting to visit.

Make the following changes in `MarketingApp`:

```js
import { mount } from 'marketing/MarketingApp';
import React, { useRef, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

export default () => {
  const ref = useRef(null);
  const history = useHistory(); // copy of the same history object used by browser and `container`

  useEffect(() => {
    mount(ref.current, {
      onNavigate: ({ pathname: nextPathname }) => {
        // console.log(nextPathname); // "/pricing"

        // Avoid infinite navigation loop
        const { pathname } = history.location;
        if (pathname !== nextPathname) {
          history.push(nextPathname); // navigate to new path
        }
      },
    });
  });

  return <div ref={ref} />;
};
```

Save changes. Click the "pricing" link in the application again, and notice that
\*\*the address in the browser bar updates to `localhost:8080/pricing`. We are
communicating the new location successfully _from_ `marketing` _to_ `container`!
