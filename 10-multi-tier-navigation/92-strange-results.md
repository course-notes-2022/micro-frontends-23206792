# Why the Strange Results?

Let's understand _why_ our routing is broken. Consider the following diagrams:

![routing diagram 1](./screenshots/routing-diagram-1.png)

1. A request for `localhost:8080/` comes in. **Two separate copies of history
   are created**: a **Browser History** and a **Memory History**. BOTH have an
   initial location value.
2. Browser History will always have a value **equal to the path** in the browser
   address bar (`/`).
3. Memory History _also_ has an _initial_ value of `/`.
4. User clicks on the `pricing` link. The **Memory History** of `marketing` is
   **updated to `/pricing`**, but **Browser History** is **NOT**. We're still at
   `/` as far as Browser History is concerned. That's why the address bar NEVER
   UPDATES from `localhost:8080`.

In other words, the **Browser History** and **Memory History** are _**not in
sync**_.

## Navigation Scoping

Links inside an app or subapp are "scoped" to their nearest parent app's router.

![navigation scoping](./screenshots/nav-scoping.png)
