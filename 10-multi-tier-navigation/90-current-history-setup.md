# Surveying Our Current History Setup

Open up `container/src/App.js`. Notice the `<BrowserRouter>` component.
Internally, it creates a copy of **Browser History** for us. This is what we
**want** in `container`.

Open the `App.js` file in `marketing`. Note that we're _also_ using
`<BrowserRouter>` here _as well, which we **don't** want_. We're going to need
to fix that to use memory history, which will **temporarily** break things.
We'll tackle that next.
