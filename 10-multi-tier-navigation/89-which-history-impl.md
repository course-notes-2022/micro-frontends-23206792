# Which History Implementation?

Let's have a quick sidebar discussion on how **routing libraries work**. There
are 2 separate parts:

1. `History`: Object to get and set the current path
2. `Router`: Shows different content based on the current path

We'll focus on the history object in this lesson.

## Kinds of History Objects

Nearly all browser routing libraries implement **3 kinds of history objects**:

1. `Browser History` (**most common**): Looks at the `path` protion of the url
   (everything after the domain) to determine the current path:

   - `http://app.com/marketing/pricing`: The path equals `marketing/pricing` in
     this case

2. `Hash History`: We won't look at this in this course

3. `Memory` or `Abstract` history: Keeps track of the current path **in
   memory**. Does not use the address in address bar **in any way**.

## What This Means for Our App

In a MFE app, recall that each project is going to have its **own** routing
library. In our app, the `container` is going to decide which app to load, and
the `subapp` is going to decide which "page"/component to show.

In our case, we're using `react-router` for the `container`, `marketing`, and
`auth` apps. We need to tell `react-router` **which kind of history we want to
use**. The most common way to achieve this in a MFE architecture is to use
**browser history** for the `container`, and **memory history** for each
`subapp`.

Why? Because each framework/version of a framework may have a different way of
implementing browser history. Importantly, the implementation not only **reads**
the history object, but **writes it as well**. If two different implementations
are writing to the **same object**, unpredicatable results and breakages can
occur! To avoid this, we make use of **memory history only** inside the
**subprojects**.
