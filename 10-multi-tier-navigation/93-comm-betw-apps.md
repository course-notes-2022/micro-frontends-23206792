# Communication Between Apps

Let's learn about two **very big topics** in MFEs:

1. Navigation
2. **Communication** between services

## What are We Trying to Achieve?

| User clicks link governed by `container`                        | User clicks link governed by `marketing`                         |
| --------------------------------------------------------------- | ---------------------------------------------------------------- |
| communicate change **down** to `marketing`                      | communicate change **up** to `container`                         |
| `marketing`'s **Memory History** should update its current path | `container`'s **Browser History** should update its current path |

Remember that communication between containers and subapps should be **as
generic as possible**! We do not want to be tied to a specific library or
version of a library! We also do not want to send **history objects** back and
forth between apps for the same reason.
