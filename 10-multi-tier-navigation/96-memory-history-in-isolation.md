# Running Memory History in Isolation

Currently the `marketing` app breaks when running in isolation, because its
`mount` function is expecting the `{onNavigate}` property, which is only passed
from the `container`. Let's fix the app so it runs in isolation again.

Make the following change in `marketing/bootstrap.js`:

```js
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { createMemoryHistory } from 'history';

const mount = (el, { onNavigate }) => {
  const history = createMemoryHistory();

  // Add conditional to pass callback from container only if onNavigate property exists
  if (onNavigate) {
    history.listen(onNavigate);
  }

  ReactDOM.render(<App history={history} />, el);
};

if (process.env.NODE_ENV === 'development') {
  const devRoot = document.querySelector('#_marketing-dev-root');

  if (devRoot) {
    // Pass empty object to mount if in development mode
    mount(devRoot, {});
  }
}

export { mount };
```
