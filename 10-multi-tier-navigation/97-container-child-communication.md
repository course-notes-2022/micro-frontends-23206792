# `container` to Subapp Communication

Let's now handle the case in which the user clicks a link in `container` and the
app communicates **down** to `marketing`.

Make the following changes in `marketing/bootstrap.js`:

```js
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { createMemoryHistory } from 'history';

// Mount function to start up the app
const mount = (el, { onNavigate }) => {
  const history = createMemoryHistory();

  if (onNavigate) {
    history.listen(onNavigate);
  }

  ReactDOM.render(<App history={history} />, el);

  // Return an object with functions the container can call
  return {
    onParentNavigate(
      // Receive path to be navigated to from parent
      { pathname: nextPathname }
    ) {
      const {
        location: { pathname },
      } = history;
      if (pathname !== nextPathname) {
        history.push(nextPathname);
      }
    },
  };
};

// If we are in development and in isolation
// call mount immediately
if (process.env.NODE_ENV === 'development') {
  const devRoot = document.querySelector('#_marketing-dev-root');

  if (devRoot) {
    mount(devRoot, {});
  }
}

// We are running through container
// and we should export the mount function
export { mount };
```

**Note that we are now _returning a value_** from the `mount` function, with a
**callback that the subapp can call** when navigation occurs in the `container`.

Update the `MarketingApp.js` file as follows:

```js
import { mount } from 'marketing/MarketingApp';
import React, { useRef, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

export default () => {
  const ref = useRef(null);
  const history = useHistory();

  useEffect(() => {
    const { onParentNavigate } = mount(ref.current, {
      onNavigate: ({ pathname: nextPathname }) => {
        const { pathname } = history.location;
        if (pathname !== nextPathname) {
          history.push(nextPathname);
        }
      },
    });

    // Set up listener on Browser history object
    // Note that both Browser history and createMemoryHistory
    // object in `marketing` have the same `listen` function
    history.listen(onParentNavigate);
  }, []); // run useEffect once upon render

  return <div ref={ref} />;
};
```
