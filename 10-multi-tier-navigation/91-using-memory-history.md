# Using Memory History

Make the following changes to the `App.js` file in the **`marketing`** project:

```js
import React from 'react';
import {
  Switch,
  Route,
  Router, // allows us to PROVIDE the history object we want
} from 'react-router-dom';
import { StylesProvider, createGenerateClassName } from '@material-ui/core';
import Landing from './components/Landing';
import Pricing from './components/Pricing';

const generateClassName = createGenerateClassName({
  productionPrefix: 'ma',
});

export default ({ history }) => {
  return (
    <div>
      <StylesProvider generateClassName={generateClassName}>
        <Router history={history}>
          <Switch>
            <Route exact path="/pricing" component={Pricing} />
            <Route path="/" component={Landing} />
          </Switch>
        </Router>
      </StylesProvider>
    </div>
  );
};
```

We will create a **copy** of the history object, and **pass it down to the
<Router>** component. Add the following to `bootstrap.js`:

```js
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { createMemoryHistory } from 'history';

const mount = (el) => {
  // Use the `createMemoryHistory` function from the `history` package
  // Create a history object and
  // pass it as a prop to the `<App>`
  const history = createMemoryHistory();
  ReactDOM.render(<App history={history} />, el);
};

if (process.env.NODE_ENV === 'development') {
  const devRoot = document.querySelector('#_marketing-dev-root');

  if (devRoot) {
    mount(devRoot);
  }
}

export { mount };
```

Save all changes and refresh the page. At localhost 8080, navigate to "Pricing".
Notice that the navigation seems to be broken. We'll look at what went wrong and
fix it in the next lesson.
