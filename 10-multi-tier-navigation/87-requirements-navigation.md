# Inflexible Requirements Around Navigation

Our application navigation is currently **broken**. If we navigate from the app
landing page to `pricing`, and back to the landing page, the **url** changes to
`localhost:8080`, but no navigation occurs.

There are many different ways of implementing navigation. **It is up to the
engineer to decide what the requirements are for the app**. Let's look at our
requirements:

## Requirement 1

> **Both the `container` and individual sub-apps need routing features.**

- Users can navigate around to different sub-aps using routing logic built into
  the `container`

- Users can navigate around _in_ a subapp using routing logic built into the
  subapp itself

- Not all subapps will require routing

## Requirement 2

> **Sub apps might need to add new pages/routes all the time.**

- New routes added to a subapp should NOT require a redeploy of the `container`!

## Requirement 3

> **We might need to show two or more microfrontends at the same time.**

- This will occur all the time if we have some kind of sidebar nav that is built
  as a separate microfrontend, for example

## Requirement 4

> **We want to use off-the-shelf routing solutions.**

- Building a routing library can be hard; we don't want to author a new one!

- _Some_ amount of custom coding is OK

## Requirement 5

> **We need navigation features for sub-apps in _both_ hosted and isolation
> modes.**

- Developing for each environment should be easy; a developer should immediately
  be able to see he/she is currently visiting.

## Requirement 6

> **If different apps need to communicate information about routing, it shuold
> be done in as generic a fashion as possible.**

- Each app might be using a completely different navigation framework

- We might swap out or upgrade navigation libraries all the time; it shouldn't
  require a rewrite of the rest of the app
