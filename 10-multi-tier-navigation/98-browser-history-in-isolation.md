# Using Browser History in Isolation

Let's make sure we can run `marketing` in isolation. We want to make use of a
Browser History object if running in isolation to make sure the URL updates in
the browser bar when clicking around in isolation.

Make the following changes to `marketing/src/bootstrap.js`:

```js
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { createMemoryHistory, createBrowserHistory } from 'history';

const mount = (
  el,
  {
    onNavigate,
    defaultHistory, // Pass a second object property, `defaultHistory`
  }
) => {
  // Set `history` = defaultHistory if not null
  const history = defaultHistory || createMemoryHistory();

  if (onNavigate) {
    history.listen(onNavigate);
  }

  ReactDOM.render(<App history={history} />, el);

  return {
    onParentNavigate({ pathname: nextPathname }) {
      const {
        location: { pathname },
      } = history;
      if (pathname !== nextPathname) {
        history.push(nextPathname);
      }
    },
  };
};

if (process.env.NODE_ENV === 'development') {
  const devRoot = document.querySelector('#_marketing-dev-root');

  if (devRoot) {
    mount(devRoot, {
      // set `defaultHistory` to return value of `createBrowserHistory` if in dev environment
      defaultHistory: createBrowserHistory(),
    });
  }
}

export { mount };
```

Save changes and refresh the `marketing` app running at `localhost:8081`. Click
the "pricing" link. Verify that the URL in the address bar reflects
`localhost:8081/pricing`. This will allow for easier development, as we can see
the URL and make changes to it. We are still using memory history in the
combined container/marketing app!
