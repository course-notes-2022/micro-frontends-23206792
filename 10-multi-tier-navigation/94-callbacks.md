# Communicating Through Callbacks

## User Clicks Link in `marketing`

1. `container` loads (calls the `mount` function)
2. `container` passes `onNavigate` callback to `marketing`
3. User clicks on the "pricing" link in `marketing`
4. Update memory History's current path to `/pricing`
5. Call `onNavigate` to tell container that the current path has changed

Make the following changes in `container/src/components/MarketingApp.js`:

```js
import { mount } from 'marketing/MarketingApp';
import React, { useRef, useEffect } from 'react';

export default () => {
  const ref = useRef(null);

  useEffect(() => {
    mount(ref.current, {
      // Pass a callback to the `mount` function as the second argument
      onNavigate: () => {
        console.log('the container noticed navigation in marketing');
      },
    });
  });

  return <div ref={ref} />;
};
```

Make the following changes to `marketing/bootstrap.js`:

```js
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { createMemoryHistory } from 'history';

// `mount` now takes a second argument; an object with a reference to a callback
const mount = (el, { onNavigate }) => {
  const history = createMemoryHistory();
  // Call the callback in `listen`
  history.listen(onNavigate);
  ReactDOM.render(<App history={history} />, el);
};

if (process.env.NODE_ENV === 'development') {
  const devRoot = document.querySelector('#_marketing-dev-root');

  if (devRoot) {
    mount(devRoot);
  }
}

export { mount };
```

**Note:** _We are making use of the `listen` fucntion that is a property of the
`history` object returned by `createMemoryHistory`. If we are NOT using React,
we would need to find another way to implement this solution using our framework
of choice._

Save the changes and restart both apps. Click the 'pricing' button in the
`marketing` subapp, and notice the output
`the container noticed navigation in marketing` in the console. Our callback is
being called successfully on navigation. Rather than logging to the console, we
can use the history object to figure out the new location. We'll look at that in
the next lesson.
