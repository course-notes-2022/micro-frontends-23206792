# Reminder on Shared Modules

Let's look at the different requests for `js` files in the `network` tab:

Note that it may _appear_ we are loading only _one_ `react-dom` dependency,
**_we are not_**. We need to refactor our app to use the `shared` modules
property as we did in our first test app in order to avoid **duplicate
dependencies**.

Add the `shared` property to the `webpack.dev.js` files for both `container` and
`marketing`:

`marketing`:

```js
const { merge } = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const commonConfig = require('./webpack.common');

// dev-specific configuration
const devConfig = {
  mode: 'development',
  devServer: {
    port: 8081,
    historyApiFallback: {
      index: 'index.html',
    },
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './public/index.html',
    }),
    new ModuleFederationPlugin({
      name: 'marketing', // Declares a global variable when our script loads in `container`
      filename: 'remoteEntry.js',
      exposes: {
        './MarketingApp': './src/bootstrap',
      },
      shared: ['react', 'react-dom'],
    }),
  ],
};

module.exports = merge(commonConfig, devConfig); // listing `devConfig` as SECOND param allows it to override any common config options with `commonConfig`
```

`container`:

```js
const { merge } = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const commonConfig = require('./webpack.common');

// dev-specific configuration
const devConfig = {
  mode: 'development',
  devServer: {
    port: 8080,
    historyApiFallback: {
      index: 'index.html',
    },
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './public/index.html',
    }),
    new ModuleFederationPlugin({
      name: 'container',
      remotes: {
        marketing: 'marketing@http://localhost:8081/remoteEntry.js',
      },
      shared: ['react', 'react-dom'],
    }),
  ],
};

module.exports = merge(commonConfig, devConfig);
```
