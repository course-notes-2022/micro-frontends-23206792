# Why Import the `mount` Function?

We now have access to the `mount` function from `marketing` inside `container`.
We need to **use** it somewhere. Recall that `mount` is a **function**, that
takes a reference to an HTML element. We can't use it as a React component.

We _could_ have `marketing` export a React component, but this would defeat the
purpose of **loose coupling** between the `container` and its child
applications. That is why we are exporting and using the `mount` function that
takes an HTML element. If at some point in the future we want to use a
**different** framework in `container` (or even in `marketing`, for that
matter), we have the flexibility to do so.
