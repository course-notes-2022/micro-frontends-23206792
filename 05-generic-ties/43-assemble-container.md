# Assembling the Container

Let's now set our `container` up. Now, our `marketing` container is running in
isolation, but as we'll see some problems will arise once we integrate it with
our `container`.

Start by copying the `marketing/config` directory to `container`. All the
configuration will be **exactly the same**, _except_ we want the dev server to
start on port `8080`.

Next, add a `start` script to the `container/package.json`:

```json
{
  "name": "container",
  "version": "1.0.0",
  "scripts": {
    "start": "webpack serve --config config/webpack.dev.js"
  }
}
```

Create `src` and `public` directories inside `container`. Add the
`public/index.html` file. This will be the **main** HTML file that will be
loaded in production or whenever we run the container application:

```html
<!DOCTYPE html>
<html>
  <head></head>
  <body>
    <div id="root"></div>
  </body>
</html>
```

Create the `src/index.js` and `src/bootstrap.js` files:

`index.js`:

```js
import('./bootstrap');
```

`bootstrap.js`:

```js
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

ReactDOM.render(<App />, document.querySelector('#root'));
```

Note that we are not creating a `mount` function here, because we **always**
want to show our `container` app **immediately** when it loads in the browser.
We don't need to do any checking of the environment and render content into
different elements, or ensure dependencies are loaded first as we do with the
sub-modules.

Start the application from a terminal and visit `localhost:8080`.
