# Assembling the App Component

Make a new file in the `src` directory, `App.js`:

```js
import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import { StylesProvider } from '@material-ui/core/styles';
import Landing from './components/Landing';
import Pricing from './components/Pricing';

export default () => {
  return (
    <div>
      <StylesProvider>
        <BrowserRouter>
          <Switch>
            <Route exact path="/pricing" component={Pricing} />
            <Route path="/" component={Landing} />
          </Switch>
        </BrowserRouter>
      </StylesProvider>
    </div>
  );
};
```

Render the `<App>` component instead of the previous `<h1>` in `bootstrap.js`:

```js
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

const mount = (el) => {
  // Render `<App/>`
  ReactDOM.render(<App />, el);
};

if (process.env.NODE_ENV === 'development') {
  const devRoot = document.querySelector('#_marketing-dev-root');

  if (devRoot) {
    mount(devRoot);
  }
}
export { mount };
```

Restart the `marketing` application and reload it in the browser window. Note
that the content has changed, and that new styling is being applied.

Next, we'll begin building the `container` to house our new applications.
