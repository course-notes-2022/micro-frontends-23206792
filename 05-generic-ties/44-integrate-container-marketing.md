# Integrate Container and Marketing

Now we need to **integrate** our `marketing` and `container` applications.

Open `marketing/config/webpack.dev.js`. We want a slightly **different**
configuration in `dev` than we do in the `prod` environment. Add a new
`ModuleFederationPlugin` configuration to the `plugins` property:

```js
const { merge } = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const commonConfig = require('./webpack.common');

const devConfig = {
  mode: 'development',
  devServer: {
    port: 8081,
    historyApiFallback: {
      index: 'index.html',
    },
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './public/index.html',
    }),
    new ModuleFederationPlugin({
      name: 'marketing', // Declares a global variable when our script loads in `container`
      filename: 'remoteEntry.js',
      exposes: {
        './Marketing': './src/bootstrap',
      },
    }),
  ],
};

module.exports = merge(commonConfig, devConfig);
```

Make `container` load the `marketing` module. Make the following changes in
`container/config/webpack.dev.js`:

```js
const { merge } = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const commonConfig = require('./webpack.common');

const devConfig = {
  mode: 'development',
  devServer: {
    port: 8080,
    historyApiFallback: {
      index: 'index.html',
    },
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './public/index.html',
    }),
    new ModuleFederationPlugin({
      name: 'container',
      remotes: {
        // key: name of module we're importing
        // value: location of remoteEntry file for that module
        marketing: 'marketing@http://localhost:8081/remoteEntry.js',
      },
    }),
  ],
};

module.exports = merge(commonConfig, devConfig);
```

Finally, we need to import `marketing` and use it in our `container` project.
Import it in the `App` file:

```js
import React from 'react';
import { mount } from 'marketing/MarketingApp';

console.log(mount);

export default () => {
  return <h1>Hello world from App!</h1>;
};
```

Restart both applications and refresh the browser windows. You should still see
the `marketing` application running in isolation, and the `mount` function
logged to the console (to demonstrate that we can successfully import the
`marketing` module into `container`).
