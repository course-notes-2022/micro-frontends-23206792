# Generic Integration

Let's now use `mount` inside `container`. We're going to need to "mess around"
with React in order to get the `mount` function to work with it.

Create a new file, `src/components/MarketingApp.js`. Add the following:

```js
import { mount } from 'marketing/MarketingApp';
import React, { useRef, useEffect } from 'react';

export default () => {
  const ref = useRef(null);

  useEffect(() => {
    mount(ref.current);
  });

  return <div ref={ref} />;
};
```

What we've done here:

1. Imported the `mount` function from `marketing`, which accepts a **reference**
   to an HTML element `el` as the only argument and renders the `marketing`
   application inside of `el`
2. Imported the `useRef` React hook and applied it to the `<div>` element to get
   a **reference** to that `<div>`
3. Imported the `useEffect` React hook and called it (once, on initial render).
   Inside, we call the `mount` function passing the `ref.current` reference to
   the `<div>` element. `mount` renders the `marketing` app inside of the
   `<div>`.

**Note that this approach gives us the flexibility** to reuse `mount` in pretty
much any context! Any other frontend framework will (probably) have a similar
method for obtaining a reference to a DOM element, which we can then pass to our
`mount` function.

Import and use `MarketingApp` inside the container `App` component:

```js
import React from 'react';
import MarketingApp from './components/MarketingApp';

export default () => {
  return (
    <div>
      <h1>Container App</h1>
      <hr />
      <MarketingApp />
    </div>
  );
};
```

Restart the applications and verify that we can still see `marketing` running in
isolation, **as well as inside our `container` "App" component**.
