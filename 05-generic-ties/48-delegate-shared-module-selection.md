# Delegating Shared Module Selection

Having to update `shared` dependencies in every application config file could
get tedious. You may **want** to do it, especially if you want to be really
specific about all the different modules being shared, and the **exact
versions** of those modules.

However, you might not have such strict requirements. In that case, there's a
quick _shortcut_ we can use.

Open `package.json` inside the `container` application. We can **require in**
the `package.json` file in our `webpack.dev` configs, and then use the
`dependencies` object to instruct Webpack to manage them for us:

`container/config/webpack.dev.js`:

```js
const { merge } = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const commonConfig = require('./webpack.common');
const packageJson = require('../package.json');

const devConfig = {
  mode: 'development',
  devServer: {
    port: 8080,
    historyApiFallback: {
      index: 'index.html',
    },
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './public/index.html',
    }),
    new ModuleFederationPlugin({
      name: 'container',
      remotes: {
        marketing: 'marketing@http://localhost:8081/remoteEntry.js',
      },
      shared: packageJson.dependencies,
    }),
  ],
};

module.exports = merge(commonConfig, devConfig);
```

Note that we have **required** our `package.json` file, and passed the
`dependencies` object defined in it as the **value** to the `shared` property in
our `ModuleFederationPlugin`. Webpack can parse this object just as it did the
array of strings we previously had.

Import and use `package.json` in the same way inside the `marketing` directory.
Save all changes and restart the applications. Verify in the developer tools
that the dependencies are no longer redundant.
